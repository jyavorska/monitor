# Baby Monitor

Needs:

- sunwait: https://github.com/risacher/sunwait
- motion monitor software: https://github.com/Motion-Project/motion/
- hub-ctrl.c: https://github.com/codazoda/hub-ctrl.c

crontab (root)

```
# reboot at sunrise to turn everything on
01 00 * * * /root/monitor/starteverything.sh

# at noon, set timers to turn everything off at sunset
01 12 * * * /root/monitor/stopeverything.sh
```
