#!/bin/bash

echo `date` starting wait for set | tee -a /var/log/monicron
sunwait wait daylight set 52.0907N 5.1214E
echo `date` finished waiting for set, starting service | tee -a /var/log/monicron
service motion stop
echo `date` finished stopping service | tee -a /var/log/monicron
