#!/bin/bash

echo `date` starting wait for rise | tee -a /var/log/monicron
sunwait wait daylight rise 52.0907N 5.1214E
echo `date` finished waiting for rise, starting service | tee -a /var/log/monicron
service motion start
echo `date` finished starting service | tee -a /var/log/monicron
